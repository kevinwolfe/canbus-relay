#include <driver/twai.h>
#include <freertos/freertos.h>
#include <freertos/task.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "debug.h"
#include "mqtt.h"
#include "nvm.h"
#include "utils.h"
#include "application.h"
#include "hardware.h"
#include "wifi.h"

static bool s_decode_mode = false;
static void _application_task(void *Param);

char mqtt_topic[64];
char mqtt_msg[256];

typedef struct
{
  int16_t voltage_100;
  int16_t current_1000;
  int16_t power_10;
} __attribute__((packed)) wave_engine_energy_report_t;

typedef struct
{
  int16_t unknown_1;
  int16_t commanded_drive_pct_100;
  int16_t unknown_2;
  int16_t rpm;
} __attribute__((packed)) wave_engine_drive_report_t;

typedef struct
{
  char *label;
  wave_engine_energy_report_t energy_report;
  wave_engine_drive_report_t drive_report;
} pump_state_t;

static pump_state_t s_pump_states[] = 
{
  { .label = "return_left" },
  { .label = "return_right" },
  { .label = "powerhead_left" },
  { .label = "powerhead_right" },
};

typedef struct
{
  int16_t bus_voltage_100;
  int16_t bus_current;
  int16_t unknown_1;
  int16_t unknown_2;
} __attribute__((packed)) controller_energy_report_t;

typedef struct
{
  char *label;
  controller_energy_report_t energy_report;
  uint64_t timestamp;
} controller_state_t;

enum
{
  CONTROLLER_CONTROLLER_X4,
  CONTROLLER_WAVE_ENGINE,
};

static controller_state_t s_controller_states[] = 
{
  [CONTROLLER_CONTROLLER_X4] = { .label = "controller_x4" },
  [CONTROLLER_WAVE_ENGINE]   = { .label = "wave_engine" },
};

float s_pH;

//-----------------------------------------------------------------------------
static void _handle_can_msg(bool extended, uint32_t id, uint8_t *p_data, uint8_t data_len);

//-----------------------------------------------------------------------------
static void _handle_can_msg(bool extended, uint32_t id, uint8_t *p_data, uint8_t data_len)
{
  if (!s_decode_mode)
  {
    if (extended)
      snprintf(mqtt_topic, sizeof(mqtt_topic), "msg/%08lx", id);
    else
      snprintf(mqtt_topic, sizeof(mqtt_topic), "msg/%03lx", id);

    mqtt_write_topic(mqtt_topic, p_data, data_len);

    return;
  }

  if (( id == 0x201 ) || (id == 0x202))
  {
    uint8_t controller_idx = ( id == 0x201 ) ? CONTROLLER_CONTROLLER_X4 : CONTROLLER_WAVE_ENGINE;
    s_controller_states[controller_idx].timestamp = *(uint64_t*)p_data;
  }

  if (( id == 0x0E040013 ) || (id == 0x0E080013))
  {
    uint8_t controller_idx = ( id == 0x0E040013 ) ? CONTROLLER_CONTROLLER_X4 : CONTROLLER_WAVE_ENGINE;
    s_controller_states[controller_idx].energy_report = *(controller_energy_report_t*)p_data;
  }

  if ( id == 0x0C040101 )
  {
    s_pH = *(float*)p_data;
  }

  if (( id >= 0x0c080A01 ) && (id <= 0x0c080A04))
  {
    uint8_t pump_idx = id - 0x0c080A01;
    s_pump_states[pump_idx].energy_report = *(wave_engine_energy_report_t*)p_data;
  }
  
  if (( id >= 0x0c080201 ) && (id <= 0x0c080204))
  {
    uint8_t pump_idx = id - 0x0c080201;
    s_pump_states[pump_idx].drive_report = *(wave_engine_drive_report_t*)p_data;
  }
}

//-----------------------------------------------------------------------------
static void _application_task(void *Param)
{
  s_decode_mode = (bool)nvm_get_param_int32(NVM_PARAM_DECODE_MSGS_MODE);
  print("Running in message %s mode\n", s_decode_mode ? "decode" : "relay");
  
	twai_message_t rx_msg;
  const twai_timing_config_t t_config = TWAI_TIMING_CONFIG_250KBITS();
	const twai_general_config_t g_config = TWAI_GENERAL_CONFIG_DEFAULT(33, 34, TWAI_MODE_NORMAL);
  const twai_filter_config_t f_config = TWAI_FILTER_CONFIG_ACCEPT_ALL();

	twai_driver_install(&g_config, &t_config, &f_config);
  twai_start();
  
  while(1)
  {
		esp_err_t ret = twai_receive(&rx_msg, pdMS_TO_TICKS(100));
		if (ret == ESP_OK)
    {
			bool extended = (bool)rx_msg.extd;
			bool remote_request_frame = (bool)rx_msg.rtr;

			if (!remote_request_frame)
      {
        print("CAN RX: 0x%lx data_length_code=%i\n", rx_msg.identifier, rx_msg.data_length_code);
        
        _handle_can_msg(extended, rx_msg.identifier, rx_msg.data, rx_msg.data_length_code);        
			}
    }

#define MQTT_UPDATE_RATE_S  10
    float now_s = system_uptime_s();
    static float last_update_s = 0;
    if ( (now_s - last_update_s ) > MQTT_UPDATE_RATE_S )
    {
      last_update_s = now_s;

      // Send pump messages
      for (uint8_t idx = 0; idx < ARRAY_SIZE(s_pump_states); idx++)
      {
        pump_state_t *p_state = &s_pump_states[idx];
        snprintf(mqtt_topic, sizeof(mqtt_topic), "wave_engine/%s", p_state->label);
        snprintf(mqtt_msg, sizeof(mqtt_msg), "{\"voltage_v\":%0.1f,\"current_a\":%0.3f,\"power_w\":%0.1f,\"drive_pct\":%0.1f,\"rpm\":%i}",
          ((float)p_state->energy_report.voltage_100) / 100,
          ((float)p_state->energy_report.current_1000) / 1000,
          ((float)p_state->energy_report.power_10) / 10,
          ((float)p_state->drive_report.commanded_drive_pct_100) / 100,
          p_state->drive_report.rpm);

        mqtt_write_topic(mqtt_topic, (uint8_t*)mqtt_msg, strlen(mqtt_msg));
        memset(&p_state->energy_report, 0, sizeof(p_state->energy_report));
        memset(&p_state->drive_report, 0, sizeof(p_state->drive_report));
      }

      // Send controller messages
      for (uint8_t idx = 0; idx < ARRAY_SIZE(s_controller_states); idx++)
      {
        controller_state_t *p_state = &s_controller_states[idx];
        snprintf(mqtt_topic, sizeof(mqtt_topic), "%s/bus_state", p_state->label);
        snprintf(mqtt_msg, sizeof(mqtt_msg), "{\"timestamp\":%lli,\"bus_voltage_v\":%0.1f,\"bus_current_a\":%0.3f}",
          p_state->timestamp,
          ((float)p_state->energy_report.bus_voltage_100) / 100,
          ((float)p_state->energy_report.bus_current));

        mqtt_write_topic(mqtt_topic, (uint8_t*)mqtt_msg, strlen(mqtt_msg));
        memset(&p_state->energy_report, 0, sizeof(p_state->energy_report));
        p_state->timestamp = 0;
      }

      // Send pH messages
      snprintf(mqtt_msg, sizeof(mqtt_msg), "%0.2f", s_pH);
      mqtt_write_topic("controller_x4/pH", (uint8_t*)mqtt_msg, strlen(mqtt_msg));
      s_pH = 0;
    }
  }
}

//-----------------------------------------------------------------------------
static uint16_t _html_add_submit_button( char *p_buffer, char *submit_value, char *button_text )
{
  return sprintf( p_buffer , 
                  "<button type=\"submit\" style=\"margin:5px;\" name=\"Action\" value=\"%s\" />%s</button>", 
                  submit_value,
                  button_text );
}

//-----------------------------------------------------------------------------
char const * application_get_html( const char *p_custom_header )
{
  static char buffer[2048] = { 0 };
  char *p_buffer = buffer;
  
  if ( p_custom_header )
  {
    p_buffer += sprintf(p_buffer, "%s", p_custom_header);
  }
  
  p_buffer += sprintf(p_buffer, "<h1>System Info</h1>");
  p_buffer += sprintf(p_buffer, "System Time: %s<br>", get_system_time_str());
  p_buffer += sprintf(p_buffer, "Firmware Build: %s %s, Boot Count: %li<br>", __DATE__, __TIME__, nvm_get_param_int32( NVM_PARAM_RESET_COUNTER ));
  p_buffer += sprintf(p_buffer, "Up-time: ");
  p_buffer += add_formatted_duration_str( p_buffer, system_uptime_s() );
  p_buffer += sprintf(p_buffer, "<br>");
  p_buffer += sprintf(p_buffer, "Operating mode: messages %s<br>", s_decode_mode ? "decoded" : "relayed");
  
  p_buffer += sprintf(p_buffer, "<hr>");
  
  p_buffer += sprintf(p_buffer, "<form action=\"/\" method=\"post\">");
  p_buffer += _html_add_submit_button( p_buffer, "toggle_mode", "Toggle operating mode" );
  p_buffer += sprintf(p_buffer, "</form>");
  
  return buffer;
}

//-----------------------------------------------------------------------------
char const * application_post_html(const char *p_post_data)
{
  // Example post data: 13%3A30&feed0_cnt=0&feed1_time=03%3A00&feed1_cnt=0&feed2_time=03%3A00&feed2_cnt=0&feed3_time=03%3A00&feed3_cnt=0&feed4_time=03%3A00&feed4_cnt=0&feed5_time=03%3A00&feed5_cnt=3
  char post_data_tmp[256];
  strncpy( post_data_tmp, p_post_data, sizeof( post_data_tmp ) );
  
  char return_html_header[256] = { 0 }; 
  char *tok = post_data_tmp;
  while (tok)
  {
    char *tag   = tok;
    char *value = strchr(tok, '=');
    tok         = strchr(tok, '&');
    
    if ( tok )
    {
     *tok++ = 0;
    }

    if (!tag || !value )
    {
      continue;
    }
    
    *value++ = 0;
    
    // Example: tag = feed3_time    
    if ( !strcmp( tag, "Action" ) && !strcmp( value, "toggle_mode" ) )
    {
      s_decode_mode = !s_decode_mode;
      sprintf( return_html_header, "<meta http-equiv=\"refresh\" content=\"10\"/><h1><b>Switching to message %s mode!</b></h1>", s_decode_mode ? "decode" : "relay" );
      nvm_set_param_int32( NVM_PARAM_DECODE_MSGS_MODE, (int32_t)s_decode_mode );
      continue;
    }
  }
  
  // Wait a little to let the application task update the next feed times
  delay_s(2);

  return application_get_html( return_html_header );
}

//-----------------------------------------------------------------------------
char const * application_get_mqtt_status_msg(void)
{
  static char status_msg[256];
  static uint32_t status_msg_id = 0;
  
  char *p_msg = status_msg;
  status_msg_id++;
  p_msg += sprintf( p_msg, "{ \"message_id\":%li,",   status_msg_id );
  p_msg += sprintf( p_msg, "\"uptime\":%lu,",         (uint32_t)system_uptime_s() ); 
  p_msg += sprintf( p_msg, "\"system_time\":\"%s\"", get_system_time_str() );
  p_msg += sprintf( p_msg, "}" );
  
  return status_msg;
}

//-----------------------------------------------------------------------------
void application_handle_mqtt_request_msg( char *p_msg )
{
  print("MQTT request message data: %s\n", p_msg );
}

//-----------------------------------------------------------------------------
void application_handle_user_button_press(void)
{
}

//-----------------------------------------------------------------------------
void application_init(void)
{
  xTaskCreate( _application_task, "app_task", 8192, NULL, 5, NULL);
}
